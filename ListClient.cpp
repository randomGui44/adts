#include <iostream>
#include "List.h"

using namespace std;

int main()
{

 List L1;

 cout << L1.size() << endl;

 L1.insert(51, 1);
 
 cout << "\nItem Found: " << L1.get(2) << endl;

 cout << "\nItem Found: " << L1.get(1) << endl;

 L1.clear();

 cout << "\nItem Found: " << L1.get(1) << endl;
 
return 0;

}
