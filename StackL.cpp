#include "StackL.h"

using namespace std;

int Stack::size(){
	return data.size();
}

int Stack::top(){
	return data.get(data.size());
}

void Stack::push(int item){
	data.insert(item, data.size()+1);
}

void Stack::pop(){
	data.remove(data.size());
}