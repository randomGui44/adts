#include "StackLL.h"

using namespace std;

class Stack::Node{
	public :
		int data = 0;
		Node* link = nullptr;
};

Stack::~Stack(){
	while(num_elements > 0){
		pop();
	}//End While
}

int Stack::size(){
	return num_elements;
}

void Stack::push(int item){
	
	Node* newPtr = new Node{item};

	newPtr->link = frontPtr;
	frontPtr = newPtr;

	num_elements++;
}

void Stack::pop(){

	Node* delPtr = frontPtr;

	frontPtr = frontPtr->link;
	
	delete delPtr;

	num_elements--;
}

int Stack::top(){

	int value = 0;

	value = frontPtr->data;

	return value;
}

void Stack::clear(){
	while(num_elements > 0){
		pop();
	}//End While
}