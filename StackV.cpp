#include "StackV.h"
#include <iostream>
#include <vector>

using namespace std;

int Stack::size(){
	return data.size();
}

void Stack::push(int item){
	data.push_back(item);
}

void Stack::pop(){
	data.pop_back();
}

int Stack::top(){
	return data[data.size()-1];
}

void Stack::clear(){
	while(data.size() != 0){
		data.pop_back();
	}//End For
}